package main

import (
	"fmt"
	"math"
	"os"
)

type circle struct {
	area float64
}

func (c circle) len() float64 {
	return 2 * math.Pi * c.ra()
}

func (c circle) ra() float64 {
	return math.Sqrt(c.area / math.Pi)
}

func (c circle) di() float64 {
	return 2 * math.Sqrt(c.area/math.Pi)
}

type rectangle struct {
	length float64
	width  float64
}

func (r rectangle) RectangleArea() float64 {
	return r.length * r.width
}

func RunRectangleArea() {
	fmt.Println("1. Напишите программу для вычисления площади прямоугольника. Длины сторон прямоугольника должны вводиться пользователем с клавиатуры.")
	r := rectangle{}
	fmt.Println("Введите длину:")
	_, err := fmt.Scanln(&r.length)
	if err != nil {
		panic(err)
	}
	fmt.Println("Введите ширину:")
	_, err = fmt.Scanln(&r.width)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Площадь прямоугольника = %v\n\n", r.RectangleArea())
}

func RunCircle() {
	fmt.Println("2. Напишите программу, вычисляющую диаметр и длину окружности по заданной площади круга. Площадь круга должна вводиться пользователем с клавиатуры.")
	c := circle{}
	fmt.Println("Введите площадь круга:")
	_, err := fmt.Scanln(&c.area)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Площадь круга = %v\n", c.area)
	fmt.Printf("Диаметр круга = %v\n", c.di())
	fmt.Printf("Длина круга = %v\n", c.len())
	fmt.Printf("Радиус круга = %v\n\n", c.ra())
}

func digit() {
	fmt.Println("3. С клавиатуры вводится трехзначное число. Выведите цифры, соответствующие количество сотен, десятков и единиц в этом числе.")
	var num int
	fmt.Println("Введите трехзначное число:")
	_, err := fmt.Scanln(&num)
	if err != nil {
		panic(err)
	}
	if (num > 999) || (num < 100) {
		fmt.Println("Число не трехзначное.")
		os.Exit(1)
	}

	fmt.Printf("Сотни:%v\n"+
		"Десятки:%v\n"+
		"Еденицы:%v\n",
		num/100, num/10%10, num%10)

}

func main() {
	RunRectangleArea()
	RunCircle()
	digit()
}
