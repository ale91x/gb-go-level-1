#!/usr/bin/env bash

echo "create dir bin"
mkdir bin
echo "go build -o ./bin main.go"
go build -o ./bin main.go
echo "go run main.go"
go run main.go
echo "./bin/main"
./bin/main

exit 0
