package main

import (
	"fmt"
)

var (
	msg = "Hello from GoLang!"
)

func main() {
	fmt.Printf("%v\n", msg)
}
